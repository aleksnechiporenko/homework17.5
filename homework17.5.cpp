﻿

#include <iostream>

class Vector
{
public:

    void Show()
    {
        std::cout << "\n" << x << " " << y << " " << z << "\n";
    }

    double VectorModule()
    {
        double countModule = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));

        return countModule;
    }

private:

    double x = 2;
    double y = 1;
    double z = 3;

}; 

int main()
{

    Vector test;
    test.Show();

    std::cout << "\n" << test.VectorModule() << "\n";
}


